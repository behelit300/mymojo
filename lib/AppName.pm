package AppName;
use Mojo::Base 'Mojolicious';
use Mojolicious::Sessions;
use Mojo::Upload;
use Mojo::Transaction::WebSocket;




sub startup {
  my $self = shift;

 # my $sessions = Mojolicious::Sessions->new;

  $self->plugin('PODRenderer');



  $self->plugin('AppName::Helpers');

  my $r = $self->routes;
  my $auth = $r->under()->to('login#check_auth');

  $r->get('/api/getuser')         ->to('api#get_users');

  $r->get('/login')               ->to('login#auth');
  $r->post('/login')              ->to('login#do_auth');
  $r->get('/logout')              ->to('login#logout');

  $auth->post('/users/add')       ->to('users#do_add');
  $auth->get('/users/add')        ->to('users#add');
  $auth->get('/users/:ID/remove') ->to('users#remove');
  $auth->post('/users/:ID/edit')  ->to('users#do_edit');
  $auth->get('/users/:ID/edit')   ->to('users#edit');
  $auth->get('/users')            ->to('users#show');

  $auth->get('/')                 ->to('main#welcome');

  
  # --- ws ---
  $auth->get('/one')               ->to('ws#one');
  $auth->get('/two')              ->to('ws#two');
  $r->websocket('/ws')           ->to('ws#init');



}

1;
