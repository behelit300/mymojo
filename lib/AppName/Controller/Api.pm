package AppName::Controller::Api;
use Mojo::Base 'Mojolicious::Controller';
use JSON;

sub get_users {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;

  my $q = 'select id, avatar, name, email, sum, updated, created from users';
  $q .= sprintf(" WHERE LOWER(name) LIKE '%s' or LOWER(email) LIKE '%s' ", lc $params->{'search'}, lc $params->{'search'})
      if ($params->{'search'});

  my $list = $self->db_arr_hash($q);

  my $json = {
  	status => 200,
  	list => $list
  };

  $self->render(json => $json);
}

1;
