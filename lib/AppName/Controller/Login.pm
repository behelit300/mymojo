package AppName::Controller::Login;
use Mojo::Base 'Mojolicious::Controller';
use Digest::MD5 'md5_hex';


sub auth {
  my $self = shift;

  $self->render(errors => {});
}

sub do_auth {
  my $self = shift;
  my $errors = {};
  $self->Validate_Form($errors);
  if (%$errors) {
 	 $self->render(template => 'login/auth', errors => $errors);
 	 return;
  }
  my $params = $self->req->body_params->to_hash; 

  my $user = $self->db_row(sprintf "select id from users where email='%s' AND pass='%s' ", $params->{'email'}, md5_hex($params->{'pass'}));

  if ($user) {
   $self->session(id => $user->{id});
   $self->redirect_to('/')
  }
  else {
  	$self->render(template => 'login/auth', errors => {other => 'Неправильный логин или пароль'});
  }
}

# ----- under: авторизован или нет -----
sub check_auth {
  my $self = shift;

  unless ($self->logged) {
     $self->redirect_to('login');
     return;
  }
  my $user = $self->user;
  $self->render(template => '/layouts/default', email => $user->{email})

}

# ----- logout -----
sub logout {
  my $self = shift;
  delete $self->session->{id};
  $self->redirect_to('/');
}

sub Validate_Form {
  my $self = shift;
  my $error = shift;
  my $params = $self->req->body_params->to_hash;  # POST

  # проверка 1
  if (!$params->{'email'}) {
    $error->{'email'} = 'Введите E-mail!'
  }

  # проверка 2
  if (!$params->{'pass'}) {
    $error->{'pass'} = 'Введите пароль!'
  }

}

1;
