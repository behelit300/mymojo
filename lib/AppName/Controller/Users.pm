package AppName::Controller::Users;
use Mojo::Base 'Mojolicious::Controller';
use Digest::MD5 'md5_hex';

# ----- Показать список пользователей -----
sub show {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;

  my $q = 'select id, avatar, name, email, sum, updated, created from users';

  $q .= sprintf(" WHERE LOWER(name) LIKE '%s' or LOWER(email) LIKE '%s' ", lc $params->{'search'}, lc $params->{'search'})
      if ($params->{'search'});

  $self->render(array => $self->db_rows($q));
}


# ----- Удаление -----
sub remove {
  my $self = shift;
  my $id = $self->stash('ID');

  $self->DB->do(sprintf "delete from users where id=%d", $id); 

  $self->flash(del_id => $id);
  $self->redirect_to('users')
}


# ----- Редактирование -----
sub edit {
  my $self = shift;
  my $id = $self->stash('ID');

  my $params = $self->db_row(sprintf "select name, email, sum, avatar from users where id=%d", $id);

  $self->render(template => 'users/form', errors => {}, value => $params, button => 'Сохранить', title => 'edit')
}

sub do_edit {
  my $self = shift;
  my $id = $self->stash('ID');

  my $params = $self->req->body_params->to_hash;  # POST

  my $error = {};
  $self->Validate_Form($error, 1);

  if (%$error) {
    $self->render(template => 'users/form', errors => $error, value => $params, button => 'Сохранить', title => 'edit');
    return;
  }

  my $filename = $self->upload_image;

  # собираем запрос
  my $q = sprintf "update `users` set name = '%s', email = '%s', sum = '%d'",
          $params->{'name'}, $params->{'email'}, $params->{'sum'};
  $q .= sprintf ", pass = '%s'", md5_hex ($params->{'pass'})
      if ($params->{'pass'});
  $q .= sprintf ", avatar = '%s'", $filename
      if ($filename);
  $q .= sprintf " where id = %d", $id;

  $self->DB->do($q);

  $self->flash(edit_email => $params->{'email'}, edit_id => $id);
  $self->redirect_to('users')

}


# ----- Добавление -----
sub add {
  my $self = shift;

  $self->render(template => 'users/form', errors => {}, value => {}, button => 'Добавить', title => 'add')
}

sub do_add {
  my $self = shift;

  my $params = $self->req->body_params->to_hash;  # POST

  my $error = {};
  $self->Validate_Form($error);
  $self->check_email($error);

  if (%$error) {
  	$self->render(template => 'users/form', errors => $error, value => $params, button => 'Добавить', title => 'add');
    return;
  }

  my $filename = $self->upload_image;

  $self->DB->do( sprintf "insert into users (name, email, pass, sum, created, avatar) values ('%s', '%s', '%s', '%d', '%s', '%s')",
  	$params->{'name'}, $params->{'email'}, md5_hex ($params->{'pass'}), $params->{'sum'}, $self->TimeMysql, $filename); 

	$self->flash(new_email => $params->{'email'});
	$self->redirect_to('users');


}


# ----- ВАЛИДАЦИЯ -----
sub Validate_Form {
  my ($self, $error, $pass_option) = @_;
  my $params = $self->req->body_params->to_hash;  # POST

  # проверка на введение NAME
  if (!$params->{'name'}) {
    $error->{'name'} = 'Введите обязательное поле'
  }

  # проверка на введение EMAIL
  if (!$params->{'email'}) {
    $error->{'email'} = 'Введите обязательное поле'
  }

  # проверка ПАРОЛЯ
  $pass_option = ($pass_option)? (length $params->{'pass'} < 6 && length $params->{'pass'} >= 1)
                               : (length $params->{'pass'} < 6);
  if ($pass_option) {
    $error->{'pass'} = sprintf 'Необходимо 6 и более символов в пароле. У вас %s', length $params->{'pass'} 
  }

  # проверка ФАЙЛА
  my $ava = $self->param('avatar');
  if ($ava->filename && !($ava->filename =~ /.(jpeg|png|jpg)$/)) {
    $error->{'avatar'} = 'Неверный формат изображения - необходим .jpeg, .png';
  }

}


# -- есть ли EMAIL в базе --
sub check_email {
  my $self = shift;
  my $error = shift;
  my $params = $self->req->body_params->to_hash;  # POST

  if ($params->{'email'}) {
    my $count = $self->DB->do(sprintf "select id from users where email='%s'", $params->{'email'});
    print "\n\n $count \n\n";

    $error->{'email'} = 'Такой email уже существует' if ($count > 0);

  }
}





1;
