package AppName::Controller::Ws;
use Mojo::Base 'Mojolicious::Controller';


my $clients = {};
my $color ='333';


sub one {
  shift->render(color => $color);
}
sub two {
  shift->render(color => $color);
}




sub init {
 my $self = shift;
 my $id = $self->tx;

 $clients->{$id} = $id;

 $self->on(message => sub {
     my ($self, $msg) = @_;

     if($msg eq 'Цвет, пожалуйста, поменяйте') {
        my @chars = ('a'..'f', '0'..'9');
        $color = join "", map { $chars[rand @chars] } 0..5;
        $clients->{$_}->send($color) for (keys %$clients);
      }
 });

 $self->on(finish => sub {
    delete $clients->{$id}
  });
}

1;
