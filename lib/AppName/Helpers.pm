package AppName::Helpers;
use base 'Mojolicious::Plugin';
use DBI;
use Digest::MD5 'md5_hex';

sub register {
	my ($self, $app) = @_;

	# авторизован?
	$app->helper(
		DB => sub {
		  my $database = 'test';
		  my $user = 'root';
		  my $password = 'root';
		  my $db = DBI->connect("DBI:mysql:$database", $user, $password);
		  $db->{mysql_enable_utf8} = 1;
		  $db->do("set names utf8");
		  return $db;
		}
	);

	$app->helper(
		logged => sub {
			shift->session->{id}
		}
	);

	# кто авторизован
	$app->helper(
		user => sub {
			my $self = shift;
			my $db = $self->DB;
  			my $query = $db->prepare(sprintf "select email from users where id = %d ", $self->session->{id});
  			$query->execute;
  			return $query->fetchrow_hashref;
		}
	);

	#запрос на 1 строку
	$app->helper(
		db_row => sub {
			my ($self, $query) = @_;
 			my $query = $self->DB->prepare($query);
 			$query->execute;
 			return $query->fetchrow_hashref;
		}
	);

	#запрос на все строки
	$app->helper(
		db_rows => sub {
			my ($self, $query) = @_;
 			my $query = $self->DB->prepare($query);
 			$query->execute;
 			return $query->fetchall_arrayref;
		}
	);
	#запрос на все строки
	$app->helper(
		db_arr_hash => sub {
			my ($self, $query) = @_;
 			my $query = $self->DB->prepare($query);
 			$query->execute;

 			my $list = ();
 			while (my $user = $query->fetchrow_hashref) {
  				push @$list, $user;
  			}
  			return $list;
		}
	);

	# загрузка аватара
	$app->helper(
 		upload_image => sub {
		  my $self = shift;
		  my $ava = $self->param('avatar');
		  return '' unless ($ava->filename);

		  my $params = $self->req->body_params->to_hash;  # POST
		  my $fname = md5_hex($params->{'email'}); # md5 е-маила
		  my $exp = $ava->filename;       # вытаскиваем расширение файла

		  $exp =~ s/.*\.//;
		  $fname = $fname.'.'.$exp;
		  $ava->move_to('../app_name/public/ava_files/'.$fname);

		  return $fname;
		}
	);

	# время в формате MySql
	$app->helper(
		TimeMysql => sub {
  			my ($sec, $min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime();
  			sprintf ("%04d:%02d:%02d %02d:%02d:%02d", $year+1900, $mon+1, $day,   $hour, $min, $sec)
		}
	);

  return $db;
};

1;